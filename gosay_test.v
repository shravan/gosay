module main

import clipboard

fn test_get_hyphens() {
	tests := [
		'haha nice', 
		'very cool isnt it?', 
		'automated tests are fun', 
		'idk what to say',
		'test driven development is quite hard ngl',
		'i like to test things',
		"www ***...hello world,,,***"
	]

	for test in tests {
		assert test.len + 2 == get_hyphens(test).len 
	}
}

fn test_copy_to_clipboard() {
	tests := [
		'haha nice',
		'very cool isnt it'
		'automated tests are fun', 
		'idk what to say',
		'test driven development is quite hard ngl',
		'i like to test things',
		"www ***...hello world,,,***"
	]

	for test in tests {
		copy_to_clipboard(test)
		mut cb := clipboard.new()
		if cb.is_available() {
			assert test == cb.paste()
		}
		cb.destroy()
	}
}