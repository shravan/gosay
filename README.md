# gosay

gosay is a CLI inspired by cowsay, but cow is replaced by the Golang Gopher.

![demo](./demo.png)

## Usage
```
gosay v0.2.0
-----------------------------------------------
Usage: gosay [options] [ARGS]

Description: Golang Gopher speaks what you'd like him to speak.


Options:
  -y, --copy                Copy the output text to clipboard.
  -c, --color <string>      Set the output color.
  -w, --write <string>      Write the output text onto a file.
  -h, --help                Print this help message.
```

Valid options for the `color` flag are red, green, blue, cyan, magenta and yellow.