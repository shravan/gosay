module main

import os
import clipboard
import flag

// colors
const (
	red     = '\033[31m'
	green   = '\033[32m'
	yellow  = '\033[33m'
	blue    = '\033[34m'
	magenta = '\033[35m'
	cyan    = '\033[36m'
	reset   = '\033[0m'
)

fn copy_to_clipboard(text string) {
	mut cb := clipboard.new()

	if cb.is_available() {
		cb.copy(text)
	} else {
		println('Clipboard is not available')
	}

	cb.destroy()
}

fn get_hyphens(s string) string {
	mut hyphens := '--'
	for _ in s {
		hyphens += '-'
	}
	return hyphens
}

fn say(text string) string {
	hyphens := get_hyphens(text)

	tmpl := "
  $hyphens
<| $text |>
  $hyphens
     \\  
	  \\    ,_---~~~~~----._         
	  _,,_,*^____      _____``*g*\"*, 
	 / __/ /'     ^.  /      \ ^@q   f 
	[  @f | @))    |  | @))   l  0 _/  
	 \`/   \~____ / __ \_____/    \   
	  |           _l__l_           I   
	  }          [______]           I  
	  ]            | | |            |  
	  ]             ~ ~             |  
	  |                            |   
	   |                           |    
	"

	return tmpl
}

fn write_to_file(filename string, text string) {
	os.write_file(filename, text) or {
		eprintln('Could not write to file!')
		exit(1)
	}
}

fn main() {
	mut fp := flag.new_flag_parser(os.args)

	// setting up the cli details
	fp.application('gosay')
	fp.version('v0.2.0')
	fp.description("Golang Gopher speaks what you'd like him to speak.")
	fp.skip_executable()

	// setting up the flags
	copy := fp.bool('copy', `y`, false, 'Copy the output text to clipboard.')
	color := fp.string('color', `c`, 'white', 'Set the output color.')
	file_to_write := fp.string('write', `w`, 'none', 'Write the output text onto a file.')
	help := fp.bool('help', `h`, false, 'Print this help message.')

	if help {
		println(fp.usage())
		return
	}

	additional_args := fp.finalize() or {
		eprintln('Unknown flag.')
		println(fp.usage())
		exit(1)
	}
	text := (additional_args.join(' '))

	gopher_text := say(text)

	if color in ['red', 'yellow', 'green', 'blue', 'cyan', 'magenta'] {
		if color == 'red' {
			println(red)
		} else if color == 'yellow' {
			println(yellow)
		} else if color == 'green' {
			println(green)
		} else if color == 'blue' {
			println(blue)
		} else if color == 'cyan' {
			println(cyan)
		} else if color == 'magenta' {
			println(magenta)
		}

		println(gopher_text)
		println(reset)
	} else {
		println(gopher_text)
	}

	if copy {
		copy_to_clipboard(gopher_text)
	}
	if file_to_write != 'none' {
		write_to_file(file_to_write, gopher_text)
	}
}
